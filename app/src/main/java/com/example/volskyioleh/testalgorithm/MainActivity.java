package com.example.volskyioleh.testalgorithm;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button editBtn = findViewById(R.id.edit_btn);
        final EditText inpuString = findViewById(R.id.input_string_edtx);
        final TextView outputString = findViewById(R.id.output_tv);
        outputString.setMovementMethod(new ScrollingMovementMethod());

        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               EditAlgorithm editAlgorithm = new EditAlgorithm(inpuString.getText().toString());
                outputString.setText(editAlgorithm.editString());
            }
        });


    }
}

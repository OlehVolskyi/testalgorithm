package com.example.volskyioleh.testalgorithm;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;


public class EditAlgorithm {
    private String mEdirString;
    private static final Set<Character> PUNCT_SET = new HashSet<>(Arrays.asList(
            '!', ',', '.', ':', ';', '?'));
    private static final Set<Character> CAPIT_SET = new HashSet<>(Arrays.asList(
            '!', '.', ';', '?'));


    public EditAlgorithm(String inputString) {
         mEdirString= inputString;

    }

    public String editString() {
        mEdirString = mEdirString.replaceAll(" +", " "); //remove extra spaces
        mEdirString = leftSpace();
        mEdirString = rightSpace();
        mEdirString = capitalizeString();
        return mEdirString;

    }


    private String leftSpace() {
        StringBuilder result = new StringBuilder(mEdirString);
        for (int i = 0; i < result.length(); i++) {
            char c = result.charAt(i);
            if (PUNCT_SET.contains(c) && i > 0) {
                if (result.charAt(i - 1) == ' ') {
                    result.deleteCharAt(i - 1);
                }
            }
        }
        return String.valueOf(result);
    }

    private String rightSpace() {
        StringBuilder result = new StringBuilder(mEdirString);
        for (int i = 0; i < result.length(); i++) {
            char c = result.charAt(i);
            if (PUNCT_SET.contains(c)) {
                if (i < result.length() - 1 && result.charAt(i + 1) != ' ') {
                    if (c == '.' && result.charAt(i + 1) == '.') {   //if c = "..."
                        continue;
                    }
                    result.insert(i + 1, " ");
                }
            }
        }
        return String.valueOf(result);
    }

    private String capitalizeString() {
        StringBuilder result = new StringBuilder(mEdirString);
        boolean capitalize = true;

        for (int i = 0; i < result.length(); i++) {
            char c = result.charAt(i);
            toLwCase(result,i,i+1);
            if (CAPIT_SET.contains(c)) {
                capitalize = true;
            } else if (capitalize && !Character.isWhitespace(result.charAt(i))) {
                toUpCase(result, i, i + 1);
                capitalize = false;
            }


        }
        return String.valueOf(result);
    }

    private void toUpCase(StringBuilder stringBuilder, int start, int end) {
        char c = Character.toUpperCase(stringBuilder.charAt(start));
        stringBuilder.replace(start, end, String.valueOf(c));
    }

    private void toLwCase(StringBuilder stringBuilder, int start, int end) {
        char c = Character.toLowerCase(stringBuilder.charAt(start));
        stringBuilder.replace(start, end, String.valueOf(c));
    }
}